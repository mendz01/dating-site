<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/getUser/{userId}', 'UsersController@getUser');
Route::get('/getUsers', 'UsersController@getUsers');
Route::post('/saveUserField', 'UsersController@saveUserField');
Route::get('/testing', 'UsersController@testing');

Route::post('/validateUser', 'UsersController@validateUser');