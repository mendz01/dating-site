import React, { Component } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { increment, decrement } from './redux/actions';
import Nav from './Nav';

export default function Test() {
    const counter = useSelector(state => state.counter);
    const dispatch = useDispatch();

    return (
        <React.Fragment>
        	<Nav />
            {counter}
            <button onClick={() => dispatch(increment())}>+</button>
            <button onClick={() => dispatch(decrement())}>-</button>
        </React.Fragment>
    );
}

