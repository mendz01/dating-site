import React, { Component, useState, useEffect } from 'react';
import axios from 'axios';
import Nav from '../Nav';
import Profile from '../Users/Profile';
import AboutMe from '../Users/AboutMe';


const Index = (props) => {
	const email = localStorage.getItem('user');
	const [count, setCount] = useState(10);
	const [userData, setUserData] = useState([]);



	useEffect(() => {
		axios.get('/api/getUser/' + email)
		  .then(response => {
		  	setUserData(response.data);
		  })
		  .catch(error => {
		    console.log(error);
		  });

		console.log('render');

		return () => {
			console.log('cleanup');
		}
	}, [email]);

	return (
		<React.Fragment>
			<Nav />

			<h1>{count}</h1>
			<button onClick={() => setCount(currentCount => { return currentCount + 1 })}>+</button>
			<button onClick={() => setCount(currentCount => { return currentCount - 1 })}>-</button>

        	<Profile userData={userData} setUserData={setUserData} isUser={true}/>

        	<hr/>

        	<AboutMe userData={userData} isUser={true}/>

		</React.Fragment>
	)
}

export default Index;