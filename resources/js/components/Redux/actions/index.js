export const increment = () => {
    return {
        type: 'INCREMENT'
    }
}
export const decrement = () => {
    return {
        type: 'DECREMENT'
    }
}

export const isLogged = () => {
	return {
		type: 'ISLOGGED'
	}
}

export const login = (email) => {
	return {
		type: 'ISLOGGED',
		payload: email
	}
}

export const logout = () => {
	return {
		type: 'ISLOGGED'
	}
}
