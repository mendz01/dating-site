import React, { Component } from 'react';
import { Redirect } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import { isLogged } from './actions';

export default function Test() {
    const logged = useSelector(state => state.isLogged);
    const dispatch = useDispatch();
    dispatch(isLogged());

    return (
        <React.Fragment>
            {logged ? 'hahaha' : 'nonnoo'}
        </React.Fragment>
    );
}

