let logged = false;
if(localStorage.getItem('user')){
  logged = true;
}

const isLogged = (state = logged, action) => {
    switch(action.type){
        case "ISLOGGED":
          return state;
       	case "LOGIN":
       		localStorage.setItem('user', action.payload);
       		return true;
       	case "LOGOUT":
       		localStorage.setItem('user', '');
       		localStorage.clear();
       		return false;
        default:
        	return state;
    }
}

export default isLogged;