import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Home from './Home/Index';
import UserProfilePage from './UserProfile';
import Login from './auth/Login';
import Register from './auth/Register';
import Test from './Test';
import MyProfile from './MyProfile';
import Redirect from './redux/redirect';

import { createStore } from 'redux';
import allReducers from './redux/reducers';
import { Provider, useSelector, useDispatch } from 'react-redux';
import { increment, decrement } from './redux/actions';

const store = createStore(allReducers);

export default function Root() {
    return (
        <Router>
            <div>
                <Switch>
                    <Route component={Home} exact path="/" />
                    <Route component={UserProfilePage} path="/profile/:id" />
                    <Route component={Login} path="/login" />
                    <Route component={Test} path="/test" />
                    <Route component={Register} path="/register" />
                    <Route component={MyProfile} path="/myProfile" />
                </Switch>
            </div>
        </Router>
    );
}


if (document.getElementById('root')) {
    ReactDOM.render(<Provider store={store}><Root /></Provider>, document.getElementById('root'));
}
