import React, { Component } from 'react';
import axios from 'axios';
import Profile from '../Users/Profile';
import AboutMe from '../Users/AboutMe';
import Nav from '../Nav';

export default class Index extends Component {
	constructor(props){
		super(props);

		this.state = {
			userId: props.match.params.id,
			userData: []
		}
	}

	componentDidMount(){
		axios.get('/api/getUser/' + this.state.userId)
		  .then(response => {
		    this.setState({ userData: response.data });
		  })
		  .catch(error => {
		    console.log(error);
		  });
	}
	
    render() {
        return (
	        <React.Fragment>
	        	<Nav />

	        	<Profile userData={this.state.userData}/>

	        	<hr/>

	        	<AboutMe userData={this.state.userData}/>
	        </React.Fragment>
        );
    }
}

