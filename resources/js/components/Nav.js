import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect
} from "react-router-dom";

export default class Nav extends Component {
    constructor(props){
        super(props);

        this.state = {
            users: [
                {id: 1, name: 'Menandro'}
            ],
            redirect: false
        }

        this.handleLogout = this.handleLogout.bind(this);
    }

    handleLogout(){
        localStorage.setItem('user', '');
        localStorage.clear();
        this.setState({ redirect: true });
    }

    render() {
        if(this.state.redirect){
            return (
                <Redirect to="/" />
            )
        }
        return (
            <div>
                <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                    <a className="navbar-brand" href="#">Dating Site</a>
                    
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarText">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item">
                                <Link className="nav-link" to="/">Home</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/test">Test</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/myProfile">My Profile</Link>
                            </li>
                        </ul>

                        <span className="navbar-text">
                            <button className="btn btn-primary" onClick={this.handleLogout}>Logout</button>
                        </span>
                    </div>
                </nav>
            </div>
        );
    }
}
