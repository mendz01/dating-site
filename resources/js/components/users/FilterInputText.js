import React, { useState, useEffect } from 'react';

const FilterInputText = (props) => {
	const {name, type, placeholder, defaultValue, onChange} = props;

	return (
		<React.Fragment>
			<div className="col-md-3">
				<div className="input-group mb-3">
				    <div className="input-group-prepend">
				      <span className="input-group-text">{placeholder}</span>
				    </div>
				    <input 
				    	type={type}
				    	className="form-control" 
				    	name={name}
				    	placeholder={placeholder}
				    	defaultValue={defaultValue} 
				    	onChange={ onChange }
				    />
			  	</div>
		  	</div>
		</React.Fragment>
	);
}

export default FilterInputText;