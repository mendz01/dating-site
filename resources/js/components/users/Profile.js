import React, { Component } from 'react';
import { InputFields } from './Fields';

// function UserCard() {
export default class Profile extends Component {
	constructor(props){
		super(props);

		this.state = {
		}
	}

	componentDidMount(){
	}
	
    render() {
        return (
			<div className="container padding">
				<div className="row">
					<div className="col-md-4">
						<img src="/source/model.jpeg" alt="placeholder 960" className="img-fluid w-100" />
					</div>

					<div className="col-md-5">
						<ul className="list-group">
							{ this.props.isUser == true ? <InputFields setUserData={this.props.setUserData} label={'Name'} name={'name'} value={this.props.userData.name}/> : <li className="list-group-item">Name: {this.props.userData.name}</li> }
							{ this.props.isUser == true ? <InputFields setUserData={this.props.setUserData} label={'Age'} name={'age'} value={this.props.userData.age} table={'UserProfile'}/> : <li className="list-group-item">Age: {this.props.userData.age}</li> }
							{ this.props.isUser == true ? <InputFields setUserData={this.props.setUserData} label={'Birthdate'} name={'birthdate'} value={this.props.userData.birthdate} table={'UserProfile'}/> : <li className="list-group-item">Birthdate: {this.props.userData.birthdate}</li> }
							{ this.props.isUser == true ? <InputFields setUserData={this.props.setUserData} label={'Horoscope'} name={'horoscope'} value={this.props.userData.horoscope} table={'UserProfile'}/> : <li className="list-group-item">Horoscope: {this.props.userData.horoscope}</li> }
							{ this.props.isUser == true ? <InputFields setUserData={this.props.setUserData} label={'Location'} name={'location'} value={this.props.userData.location} table={'UserProfile'}/> : <li className="list-group-item">Location: {this.props.userData.location}</li> }
							{ this.props.isUser == true ? <InputFields setUserData={this.props.setUserData} label={'Height'} name={'height'} value={this.props.userData.height} table={'UserProfile'}/> : <li className="list-group-item">Height: {this.props.userData.height}</li> }
							{ this.props.isUser == true ? <InputFields setUserData={this.props.setUserData} label={'Weight'} name={'weight'} value={this.props.userData.weight} table={'UserProfile'}/> : <li className="list-group-item">Age: {this.props.userData.weight}</li> }
						</ul>
					</div>

					<div className="col-md-3">
						<button><i className="fa fa-video-camera fa-3x" aria-hidden="true"></i></button>
					</div>
				</div>
			</div>

        );
    }
}

