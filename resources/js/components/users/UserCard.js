import React, { Component } from 'react';
import axios from 'axios';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

// function UserCard() {
export default class UserCard extends Component {
	constructor(props){
		super(props);

		this.state = {
			userId: props.userId,
			userData: []
		}

		this.handleViewProfile = this.handleViewProfile.bind(this);
	}

	componentDidMount(){
		axios.get('/api/getUser/' + this.state.userId)
		  .then(response => {
		    this.setState({ userData: response.data });
		  })
		  .catch(error => {
		    console.log(error);
		  });
	}

	handleViewProfile(){
		console.log('hahaaha');
	}
	
    render() {
        return (
	        <div className="col-md-3">
	            <div className="card">
	                <div className="card-header">{this.state.userData.name}</div>

	                <div className="card-body">
	                	<img className="card-img-top" src="/source/model.jpeg"/>
    					<p className="card-text">It's a broader card with text below as a natural lead-in to extra content. This content is a little longer.</p>

    					<div className="text-center">
    						<Link className="btn btn-primary" to={"/profile/" + this.state.userId}>View Profile</Link>
    					</div>
	                </div>
	            </div>
	        </div>
        );
    }
}

