import React, { useState, useEffect } from 'react';


const InputFields = (props) => {
	const userEmail = localStorage.getItem('user');
	const [enabled, setEnabled] = useState(false);

	const [inputValue, setInputValue] = useState(props.value);

	const handleSave = () => {
		if(enabled){
			axios.post('/api/saveUserField', {email: userEmail,name: props.name, value: inputValue, table: props.table})
				.then(response =>{
					// props.setUserData(response.data);
					props.setUserData(currentData => {
						return {...currentData, [props.name]: inputValue}
;					});
				})
				.catch(error => {
					console.log(error);
				});

			setEnabled(false);
		} else {
			setEnabled(true);
		}
	}

	return (
		<div className="input-group mb-3">
		    <div className="input-group-prepend">
		      <span className="input-group-text">{props.label}</span>
		    </div>
		    <input 
		    	type="text" 
		    	className="form-control" 
		    	placeholder={props.name}
		    	defaultValue={props.value} 
		    	disabled = {enabled ? "" : "disabled"}
		    	onChange={ (e) => setInputValue(e.target.value) }
		    />
		    <button className="btn btn-primary" onClick={ handleSave }>
		    	{enabled ? 'Save' : 'Edit'}
		    </button>
	  	</div>
	)
}

const InputFields2 = () => {
	return (
		<div>
			uuuuuuuu
		</div>
	)
}

export {
  InputFields,
  InputFields2,
}

