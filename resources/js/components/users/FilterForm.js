import React, { useState, useEffect } from 'react';
import axios from 'axios';

import FilterInputText from './FilterInputText';

const FilterForm = (props) => {
	const [filters, setFilters] = useState({});
	const [showFilters, setShowFitlers] = useState(false);

	useEffect(() => {
		// console.log(filters);
        // axios.get('/api/getUsers', {name: filters.name, age: filters.age})
        //   .then(response => {
        //   	// props.filterUsers(response.data);
        //   	// console.log(response.data);
        //   	console.log(filters);
        //   })
        //   .catch(error => {
        //     console.log(error);
        //   });
	}, [filters]);

	const applyFilters = async () => {
		let url = `/api/getUsers?page=${props.activePage}`;
        let response = await axios.get(url, {params: filters});

        let {data} = response.data;

        console.log('filter');
        console.log(data);
        props.filterUsers(data);
	}




	return (
		<div className="container-fluid">

			<div className="row mt-2 mb-2 ml-3">
				<button className="btn btn-primary" data-toggle="collapse" data-target="#product-images" onClick={ () => setShowFitlers(!showFilters) }>{showFilters ? 'Hide Filters' : 'View Filters'}</button>
			</div>

			<div id="product-images" className="collapse">
				<div className="row mt-3 mb-5">

					<FilterInputText name="name" type="text" placeholder="Name" defaultValue="" onChange={e => setFilters( {...filters, [e.target.name]: e.target.value} )} />
					<FilterInputText name="min_age" type="number" placeholder="Minimum Age" defaultValue="" onChange={e => setFilters( {...filters, [e.target.name]: e.target.value} )} />
					<FilterInputText name="max_age" type="number" placeholder="Maximum Age" defaultValue="" onChange={e => setFilters( {...filters, [e.target.name]: e.target.value} )} />
				</div>
				<div className="row">
					<button onClick={ applyFilters } className="btn btn-warning">Apply Filters</button>
				</div>
			</div>

		</div>
	
	);
}

export default FilterForm;