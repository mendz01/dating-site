import React, { Component } from 'react';
import axios from 'axios';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

// function UserCard() {
export default class AboutMe extends Component {
	constructor(props){
		super(props);

		this.state = {
		}
	}

	componentDidMount(){
	}
	
    render() {
        return (
			<div className="container padding">
				<div className="row">
					<div className="col-md-3">
						<ul className="list-group">
							<li className="list-group-item">Name: {this.props.userData.name}</li>
							<li className="list-group-item">Age: {this.props.userData.age}</li>
							<li className="list-group-item">Birthdate: {this.props.userData.birthdate}</li>
							<li className="list-group-item">Horoscope: {this.props.userData.horoscope}</li>
							<li className="list-group-item">Location: {this.props.userData.location}</li>
							<li className="list-group-item">Height: {this.props.userData.height}</li>
							<li className="list-group-item">Weight: {this.props.userData.weight}</li>
						</ul>
					</div>

					<div className="col-md-9">
						<p>{this.props.userData.short_description}</p>
					</div>
				</div>
			</div>

        );
    }
}