import React, { Component } from 'react';
import axios from 'axios';
import ReactDOM from 'react-dom';
import { Redirect } from "react-router-dom";
import Pagination from "react-js-pagination";

import Root from '../Root';
import UserCard from '../Users/UserCard';
import Nav from '../Nav';
import Test from '../Test';
import FilterForm from '../Users/FilterForm';

export default class Index extends Component {
    constructor(props){
        super(props);

        this.state = {
            users: [
                // {id: 1, name: 'Menandro'}
            ],
            activePage: 1,
            total: 0,
            per_page: 1,
            last_page: 0
        }

        this.getUsers = this.getUsers.bind(this);
        this.filterUsers = this.filterUsers.bind(this);
    }

    async getUsers(pageNumber){
        let url = `/api/getUsers?page=${pageNumber}`;
        let response = await axios.get(url);

        let {data, activePage, total, per_page, last_page} = response.data;

        this.setState({users: data, 
                        total: total, 
                        per_page: per_page, 
                        last_page: last_page});
    }

    componentDidMount(){
        this.getUsers(this.state.activePage);
        // axios.get('/api/getUsers')
        //   .then(response => {
        //     this.setState({users: response.data});
        //   })
        //   .catch(error => {
        //     console.log(error);
        //   });
    }

    handlePageChange(pageNumber) {
        this.getUsers(pageNumber);
        this.setState({activePage: pageNumber});
    }


    filterUsers(filteredUsers){
        this.setState({users: filteredUsers});
    }

    render() {
        if(!localStorage.getItem('user')){
            return (
                <Redirect to="/login" />
            )
        }

        return (
            <React.Fragment>
                <Nav />

                <FilterForm users={this.state.users} filterUsers={this.filterUsers} activePage={this.state.activePage} />

                <div className="container">
                    <div>
                        <h2>Meet New People</h2>
                    </div>
                    <div className="row">
                        {
                            this.state.users.map(user => {
                                return <UserCard key={user.id} userId={user.id}/>
                            })
                        }
                    </div>

                    <div className="mt-3">
                        <Pagination
                        activePage={this.state.activePage}
                        itemsCountPerPage={this.state.per_page}
                        totalItemsCount={this.state.total}
                        onChange={this.handlePageChange.bind(this)}
                        pageRangeDisplayed={10}
                        innerClass={'pagination'}
                        itemClass={'page-item'}
                        linkClass={'page-link'}
                        />
                    </div>
                </div>
            </React.Fragment>
        );
    }
}
