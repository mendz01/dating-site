import React, { Component } from 'react';
import axios from 'axios';
import { Redirect } from "react-router-dom";
import { Link } from "react-router-dom";

// function UserCard() {
export default class Login extends Component {
	constructor(props){
		super(props);

		this.state = {
			email: '',
			password: '',
			redirect: false,
			errorMessage: ''
		}

		this.handleOnChange = this.handleOnChange.bind(this);
		this.handleOnSubmit = this.handleOnSubmit.bind(this);
		this.clearOnClick = this.clearOnClick.bind(this);
	}

	componentDidMount(){
	}

	handleOnChange(e){
		this.setState({ [e.target.name]: e.target.value });
	}

	handleOnSubmit(e){
		e.preventDefault();

		axios.post('/api/validateUser', { email: this.state.email, password: this.state.password })
		  .then(response => {
		  	console.log(response);
		  	if(response.data.error){
		  		this.setState({ errorMessage: response.data.error });
		  	} else{
				localStorage.setItem('user', this.state.email);
				this.setState({ redirect: true });
		  	}
		  })
		  .catch(error => {
		    console.log(error);
		  });
	}

	clearOnClick(e){
		this.setState({ [e.target.id]: '' });
	}
	
    render() {
    	if(this.state.redirect){
    		return (
    			<Redirect to="/"/>
    		);
    	}

        return (
			<div className="container">
			    <div className="row justify-content-center">
			        <div className="col-md-8">
			            <div className="card">
			                <div className="card-header">Login</div>

			                <span className="alert-danger" id="errorMessage" onClick={this.clearOnClick}>{this.state.errorMessage}</span>

			                <div className="card-body">
			                    <form onSubmit={this.handleOnSubmit}>
			                        <div className="form-group row">
			                            <label className="col-md-4 col-form-label text-md-right">E-Mail Address</label>

			                            <div className="col-md-6">
			                                <input id="email" type="email" onChange={this.handleOnChange} className="form-control" name="email" required />
			                            </div>
			                        </div>

			                        <div className="form-group row">
			                            <label className="col-md-4 col-form-label text-md-right">Password</label>

			                            <div className="col-md-6">
			                                <input id="password" type="password" onChange={this.handleOnChange} className="form-control" name="password" required />
			                            </div>
			                        </div>

			                        <div className="form-group row">
			                            <div className="col-md-6 offset-md-4">
			                                <div className="form-check">
			                                    <input className="form-check-input" type="checkbox" name="remember" id="remember" />

			                                    <label className="form-check-label" >
			                                        Remember Me
			                                    </label>
			                                </div>
			                            </div>
			                        </div>


			                        <div className="form-group row mb-0">
			                            <div className="col-md-6">
			                                <button type="submit" className="btn btn-primary">
			                                    Login
			                                </button>
			                            </div>
			                            <div className="col-md-6">
			                            	<Link className="btn btn-primary" to="/register">Register</Link>
			                            </div>
			                        </div>
			                    </form>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
        );
    }
}

