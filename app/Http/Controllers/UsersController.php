<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Builder;
use App\User;
use App\UserProfile;
use DB;
use Log;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getUsers(Request $request)
    {
        // $users = User::all();

        // return json_encode($users);
        $query_builder = DB::table('users as u')
                        ->join('user_profiles as up', 'u.id', '=', 'up.user_id');


        if(isset($request->name) && !empty($request->name)){
            $query_builder->where('u.name', 'like', '%'.$request->name.'%');
        }

        if(isset($request->min_age) && !empty($request->min_age)){
            $query_builder->where('up.age', '>=', $request->min_age);
        }

        if(isset($request->max_age) && !empty($request->max_age)){
            $query_builder->where('up.age', '<=', $request->max_age);
        }

        $users = $query_builder->paginate(8);

        return response($users, 200);
    }

    public function getUser($userId)
    {
        $users = User::where('email', $userId)->first();

        if(!$users){
            $users = User::find($userId);
        }
        
        $users->age = $users->userProfile->age;
        $users->birthdate = $users->userProfile->birthdate;
        $users->horoscope = $users->userProfile->horoscope;
        $users->location = $users->userProfile->location;
        $users->height = $users->userProfile->height;
        $users->weight = $users->userProfile->weight;
        $users->short_description = $users->userProfile->short_description;


        return json_encode($users);
    }

    public function validateUser(Request $request){
        $user = User::where('email', $request->email)
                    ->first();

        if($user){
            if(Hash::check($request->password, $user->password)){
                return json_encode($user);
            } else {
                return json_encode(array('error' => 'Incorrect Password'));
            }
            
        } else {
            return json_encode(array('error' => 'User not Found.'));
        }
    }

    public function saveUserField(Request $request){
        $user = User::where('email', $request->email)
                    ->first();

        if($request->table == 'UserProfile'){
            $model = UserProfile::where('user_id', $user->id)->first();
        } else {
            $model = $user;
        }

        $model->{$request->name} = $request->value;
        $model->save();

        return json_encode($model);
    }

    public function testing(){
        // $user = User::find(1);
        $user = User::where('email', 'menandrodelosreyes@gmail.com')
                    ->first();

        $user->{'name'} = 'kkk';

        $user->save();

        return json_encode($user);
    }
}
